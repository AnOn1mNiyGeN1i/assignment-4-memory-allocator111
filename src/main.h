#ifndef _MAIN_H
#define _MAIN_H
#include "./mem.h"
#include "mem_internals.h"
#include <math.h>
#ifndef N
#define N 50
#endif
#ifndef M
#define M 20
#endif

const int64_t LOCAL = 200;

bool test_free();
bool test_malloc();

#endif
